package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void shouldReturnFive(){
        assertEquals( "Testing method echo() in App class", 5, dawson.App.echo(5) );
    }

    @Test
    public void shouldResturnOneMore(){
        assertEquals( "Testing method oneMore() in App class", 3, dawson.App.oneMore(3) );
    }
}
